@echo off

set RESOURCE_NAME=apache-ant-1.8.4

echo.
echo ------------------------------------------------
echo  Installing %RESOURCE_NAME%                             
echo ------------------------------------------------

echo Downloading...
wget http://apache.cu.be/ant/binaries/apache-ant-1.8.4-bin.zip -O %INSTALLER_WORK%\%RESOURCE_NAME%.zip
if errorlevel 1 ( goto failure )

echo Removing previous installation...
rd /S /Q %WORKBENCH_TOOLS%\%RESOURCE_NAME%
if errorlevel 1 ( goto failure )

echo Unpacking...
unzip -o %INSTALLER_WORK%\%RESOURCE_NAME%.zip -d %WORKBENCH_TOOLS%
if errorlevel 1 ( goto failure )

:succes
echo %RESOURCE_NAME% installation succesful                  
echo.
goto end

:failure
echo %RESOURCE_NAME% installation failed                  
echo.
exit /b %errorlevel%
    
:end

del /Q %INSTALLER_WORK%\%RESOURCE_NAME%.zip
